package com.example.manualcopy

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.manual_copy_edit,menu)
        return super.onCreateOptionsMenu(menu)
    }
}
